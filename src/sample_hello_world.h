#pragma once

namespace sample {

struct HelloWorld {
  template<class OS>
  friend OS& operator<<(OS& os, const HelloWorld&) {
    os << "Hello World!";
    return os;
  }
};

} // sample
