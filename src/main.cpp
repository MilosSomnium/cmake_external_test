#include "sample_hello_world.h"

#include "Utils/Callstack.h"

#include <boost/program_options.hpp>
#include <fmt/format.h>
#include <iostream>

// Наше приложение будет иметь один параметр командной строки - "--help"
auto parseArgs(int argc, char* argv[]) {
  namespace po = boost::program_options;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "Produce this message")
    ("stack,s", "print callstack")
    ("format,f", "test format");

  auto parsed = po::command_line_parser(argc, argv)
    .options(desc)
    .allow_unregistered()
    .run();

  po::variables_map vm;
  po::store(parsed, vm);
  po::notify(vm);

  return std::pair(vm, desc);
}

int main(int argc, char* argv[]) 
try {
  auto [vm, desc] = parseArgs(argc, argv);

  if (vm.count("help")) {  
    std::cout << desc << std::endl;
    return 0;
  }
  if (vm.count("stack")) {
      std::cout << "callstack: " << getStacktrace() << std::endl ;
  }
  if(vm.count("format"))
  {
      std::cout << fmt::format("The answer is {}", 42) << std::endl;
  }

  std::cout << sample::HelloWorld{} << std::endl;

  return 0;
} catch (std::exception& e) {
  std::cerr << "Unhandled exception: " << e.what() << std::endl;
  return -1;
}