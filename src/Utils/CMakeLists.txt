
set(SOURCES
    Callstack.h
    Callstack.cpp
)

add_library(Utils ${SOURCES})
target_link_libraries(Utils PUBLIC Boost::stacktrace)