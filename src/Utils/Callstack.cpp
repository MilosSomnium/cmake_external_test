#include "Callstack.h"

//#include <boost/stacktrace/stacktrace.hpp>

#include <boost/stacktrace.hpp>

std::string getStacktrace()
{
    return to_string(boost::stacktrace::stacktrace());
}